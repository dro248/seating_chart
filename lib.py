from itertools import combinations
from math import sqrt
from functools import reduce
from collections import namedtuple
from copy import copy
import logging
import pandas as pd
from IPython.display import display


class SeatingChart:
    def __init__(self, tm_list, shape):
        self.tm_list = tm_list

        ChartShape = namedtuple('ChartShape', ('x', 'y'))
        self.shape = ChartShape(x=shape[0], y=shape[1])
        print(self.shape)

        
    @staticmethod
    def calc_dist(pt1, pt2):
        """Simple Pythagorean Theorem to calculate the distance between 2 points"""
        return sqrt(abs(pt1[0] - pt2[0])**2 + abs(pt1[1] - pt2[1])**2)


    def calc_score(self, tm_list):
        """
        Calculate and returns the SUM of the distances between all teammates (TMs)
        """
        return reduce(lambda a, b: a+b, map(lambda x: self.calc_dist(x[0], x[1]), combinations(tm_list, r=2)))


    def show_matrix(self, tm_list):
        matrix = [['_' for _ in range(self.shape.x)] for _ in range(self.shape.y)]

        # place TMs in the matrix (as 1's)
        for t in tm_list:
            matrix[t.x][t.y] = 'X'
        
        df = pd.DataFrame(matrix)
        display(df)


    @staticmethod
    def location_generator(x, y):
        Point = namedtuple('Point', ('x', 'y'))

        for _x in range(x):
            for _y in range(y):
                yield Point(x=_x, y=_y)


    def optimize(self, verbose=False):
        # while our current score is still getting smaller, continue loop
        best_config_so_far = copy(self.tm_list)
        best_score_so_far = self.calc_score(tm_list=best_config_so_far)

        print(f'best_config_so_far: {best_config_so_far}')
        print(f'score: {best_score_so_far}')
        self.show_matrix(best_config_so_far)

        continue_searching = True
        global_loops = 0

        while continue_searching:
            # We are immediately setting continue_searching to False: we want it to stop as soon as it stops finding better solutions
            continue_searching = False

            print(f'Global loops: {global_loops}')
            global_loops +=1

            # loop thru TMs
            for idx, tm in enumerate(self.tm_list):

                # to make things clean, create a generator of locations (x, y) which loops thru all locations
                locations = self.location_generator(self.shape.x, self.shape.y)
                
                for loc in locations:

                    # loc is now a new location that we would like to use                   
                    if loc in set(best_config_so_far):
                        # if a teammate is already in that location, SWAP
                        # TODO: for now, we'll just skip
                        continue

                    else:
                        # No teammates in this location! 
                        # 1. create a current_config:
                        #   - this is the `best_config_so_far` but with coordinates of the current TM switched to our current location
                        current_config = copy(best_config_so_far)
                        current_config[idx] = loc

                        # 2. Calculate the score of the current config
                        current_score = self.calc_score(tm_list=current_config)

                        # 3. Set if current score is better, we have found a new best_(config|score)_so_far
                        if current_score < best_score_so_far:
                            # current_config / score are now the new best!
                            best_config_so_far = current_config
                            best_score_so_far = current_score

                            continue_searching = True

                            # if verbose:
                            #     print(f'best_config_so_far: {best_config_so_far}')
                            #     print(f'score: {best_score_so_far}')

                if verbose:
                    print(f'best_config_so_far: {best_config_so_far}')
                    print(f'score: {best_score_so_far}')
                    self.show_matrix(best_config_so_far)        

        print()
        print('-'*30)
        print(f'best_config_so_far: {best_config_so_far}')
        print(f'score: {best_score_so_far}')
        print('-'*30)

        self.show_matrix(best_config_so_far)


if __name__ == '__main__':
    Point = namedtuple('Point', ('x', 'y'))

    tm_list = [
        Point(0, 0),
        Point(0, 1),
        # Point(1, 0),
        Point(1, 2),
        # Point(2, 2),
    ]

    sc = SeatingChart(tm_list=tm_list, shape=(3, 3))
    sc.optimize()
