from collections import namedtuple
import itertools
from functools import reduce
from lib import calc_dist


Point = namedtuple('Point', ('x', 'y'))

tm_list = [
    Point(0, 0),
    Point(0, 1),
    Point(1, 0),
    Point(1, 2),
    Point(2, 2),
]

# # Generates the list of edges (combinations)
# for x in itertools.combinations('ABCD', r=2):
#     print(x)


l = [
    [0, 0],
    [0, 1],
]

# l1 = reduce(lambda a, b: a+b, map(lambda x: abs(x[0] - x[1]), l))
print(list(itertools.combinations(tm_list, r=2)))

r = reduce(lambda a, b: a+b, map(lambda x: calc_dist(x[0], x[1]), itertools.combinations(tm_list, r=2)))

print(r)
